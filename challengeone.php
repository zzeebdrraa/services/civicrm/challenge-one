<?php

require_once 'challengeone.civix.php';
require_once 'CRM/Challengeone/challengeone_hooks.php';

// phpcs:disable
use CRM_Challengeone_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function challengeone_civicrm_config(&$config): void {
  _challengeone_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function challengeone_civicrm_install(): void {
  _challengeone_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function challengeone_civicrm_enable(): void {
  _challengeone_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_pre().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function challengeone_civicrm_pre($op, $objectName, $objectId, &$objectRef): void {
  challengeone_civicrm_pre_impl($op, $objectName, $objectId, $objectRef);
}
