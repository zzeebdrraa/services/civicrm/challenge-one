<?php

function generate_one_of_prefix($count) {
  if ( $count === null || strlen($count) == 0 ) return false;
  return "(one of " . $count . ")";
}

function get_last_string_part($delimiter, $str) {
  if ( $delimiter === null || $str === null ) return false;
  if ( strlen($delimiter) == 0  || strlen($str) == 0 ) return $str;

  $tmp = explode($delimiter, $str);
  return end($tmp);
}

function prepend_to_string($prefix, $delimiter, $str) {
  if ( $prefix === null || $delimiter === null || $str === null ) return false;
  return $prefix . $delimiter . $str;
}

?>
