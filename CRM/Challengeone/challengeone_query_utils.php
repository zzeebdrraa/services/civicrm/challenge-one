<?php

function query_contact_first_name_occurence($firstName) {
  if ( $firstName === null ) return false;

  $count = CRM_Core_DAO::singleValueQuery("
    SELECT COUNT(*)
    FROM civicrm_contact
    WHERE first_name = %1", [ 1 => [$firstName, 'String']]
  );

  if ( $count === null ) return false;
  return $count;
}

?>
