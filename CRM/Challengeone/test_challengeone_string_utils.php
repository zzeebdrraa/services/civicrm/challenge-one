<?php

use PHPUnit\Framework\TestCase;

require_once 'challengeone_string_utils.php';

class test_challengeone_string_utils extends TestCase {

  public function testGenerateOneOfPrefix() {
    $expected = '(one of 5)';

    $result = generate_one_of_prefix("5");
    $this->assertEquals($expected, $result);
  }

  public function testGenerateOneOfPrefixEmptyArg() {
    $expected = false;

    $result = generate_one_of_prefix('');
    $this->assertEquals($expected, $result);
  }

  public function testGenerateOneOfPrefixMissingArgs() {
    $expected = false;

    $result = generate_one_of_prefix(null);
    $this->assertEquals($expected, $result);
  }

  public function testGetLastStringPart() {
    $expected = 'four';

    $result = get_last_string_part(" ", "one two three four");
    $this->assertEquals($expected, $result);
    $result = get_last_string_part("--", "one--two--three--four");
    $this->assertEquals($expected, $result);
    $result = get_last_string_part(" ", "(one two three) four");
    $this->assertEquals($expected, $result);

  }

  public function testGetLastStringPartNoMatchingDelimiter() {
    $expected = 'one two three four';

    $result = get_last_string_part("-", "one two three four");
    $this->assertEquals($expected, $result);
  }

  public function testGetLastStringPartemptyArgs() {
    $expected = 'one two three four';

    $result = get_last_string_part('', "one two three four");
    $this->assertEquals($expected, $result);

    $result = get_last_string_part('-', "");
    $this->assertEquals('', $result);

    $result = get_last_string_part('', '');
    $this->assertEquals('', $result);
  }

  public function testGetLastStringPartMissingArgs() {
    $expected = false;

    $result = get_last_string_part(null, "one two three four");
    $this->assertEquals($expected, $result);
    $result = get_last_string_part("--", null);
    $this->assertEquals($expected, $result);
    $result = get_last_string_part(null, null);
    $this->assertEquals($expected, $result);
  }

  public function testPrependToString() {
    $expected = 'prefix-string';
    $result = prepend_to_string('prefix', '-', 'string');

    $this->assertEquals($expected, $result);
  }

  public function testPrependToStringEmptyArgs() {
    $result = prepend_to_string('', '-', 'string');
    $this->assertEquals('-string', $result);
    $result = prepend_to_string('prefix', '', 'string');
    $this->assertEquals('prefixstring', $result);
    $result = prepend_to_string('prefix', '-', '');
    $this->assertEquals('prefix-', $result);
    $result = prepend_to_string('', '', '');
    $this->assertEquals('', $result);
  }

  public function testPrependToStringMissingArgs() {
    $expected = false;

    $result = prepend_to_string(null, '-', 'string');
    $this->assertEquals($expected, $result);
    $result = prepend_to_string('prefix', null, 'string');
    $this->assertEquals($expected, $result);
    $result = prepend_to_string('prefix', '-', null);
    $this->assertEquals($expected, $result);
    $result = prepend_to_string(null, null, null);
    $this->assertEquals($expected, $result);
  }
}
