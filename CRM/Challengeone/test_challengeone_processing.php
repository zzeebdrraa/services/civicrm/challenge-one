<?php

use PHPUnit\Framework\TestCase;

require_once 'challengeone_processing.php';

function mock_query_count() {
  return "5";
}

function mock_query_null() {
  return null;
}

function mock_logger() {
}

class test_challengeone_processing extends TestCase {

  public function testProcessing() {
    $expected = '(one of 5) Rose';

    $callableQuery = mock_query_count(...);
    $callableLogger = mock_logger(...);
    $obj = array("first_name" => "Rose");
    challengeone_processing("edit", "Individual", "81", $obj, $callableQuery, $callableLogger);

    $this->assertEquals($expected, $obj["first_name"]);
  }

  public function testProcessingNoIndividual() {
    $expected = 'Rose';

    $callableQuery = mock_query_count(...);
    $callableLogger = mock_logger(...);
    $obj = array("first_name" => "Rose");
    challengeone_processing("edit", "Other", "81", $obj, $callableQuery, $callableLogger);

    $this->assertEquals($expected, $obj["first_name"]);
  }

  public function testProcessingNoFirstName() {
    $expected = '';

    $callableQuery = mock_query_count(...);
    $callableLogger = mock_logger(...);
    $obj = array();
    challengeone_processing("edit", "Individual", "81", $obj, $callableQuery, $callableLogger);

    $this->assertEmpty($obj);
  }

  public function testProcessingNoOpEdit() {
    $expected = 'Rose';

    $callableQuery = mock_query_count(...);
    $callableLogger = mock_logger(...);
    $obj = array("first_name" => "Rose");
    challengeone_processing("new", "Individual", "81", $obj, $callableQuery, $callableLogger);

    $this->assertEquals($expected, $obj["first_name"]);
  }

  public function testProcessingNoCallable() {
    $expected = 'Rose';

    $callableQuery = mock_query_count(...);
    $callableLogger = mock_logger(...);
    $obj = array("first_name" => "Rose");

    challengeone_processing("new", "Individual", "81", $obj, null, $callableLogger);
    $this->assertEquals($expected, $obj["first_name"]);
    challengeone_processing("new", "Individual", "81", $obj, $callableQuery, null);
    $this->assertEquals($expected, $obj["first_name"]);
  }
}

?>
