<?php

require_once "challengeone_processing.php";
require_once "challengeone_query_utils.php";

function log_error($str) {
  if( strlen($str)) Civi::log()->error($str);
}

function challengeone_civicrm_pre_impl($op, $objectName, $objectId, &$objectRef) {
  $callableQuery = query_contact_first_name_occurence(...);
  $callableLogger = log_error(...);
  return challengeone_processing($op, $objectName, $objectId, $objectRef, $callableQuery, $callableLogger);
}

?>
