<?php

require_once "challengeone_string_utils.php";

/*
  Example of an object of type Individual

  objectName == Individual
  objectRef = Array (
    [qfKey] => CRMContactFormInlineContactName1ygyi4jo5tog8cgg4o0osc0ks440sogwsksg0o08k4gow4so8g_3178
    [entryURL] => http://demo-drupal-10-civicrm-5-67-1-www.localhost/civicrm/ajax/inline?cid=12&amp;class_name=CRM_Contact_Form_Inline_ContactName&amp;reset=1
    [oplock_ts] => 2023-11-28 11:12:51
    [_qf_default] => ContactName:upload
    [MAX_FILE_SIZE] => 2097152
    [_qf_ContactName_upload] => 1 
    [prefix_id] =>
    [first_name] => Lawerence
    [middle_name] => K
    [last_name] => Adams
    [suffix_id] =>
    [contact_type] => Individual
    [contact_id] => 12 )
*/
function challengeone_processing($op, $objectName, $objectId, &$objectRef, $callableQuery, $callableErrorLogger) {

  // make sure we only use an Individual contact that has been edited
  if ( $objectName != "Individual" || $op != "edit" ) return;
  if ( $callableQuery === null || $callableErrorLogger == null ) return;

  $firstName = $objectRef['first_name'] ?? null;
  if ( $firstName === null ) {
    $callableErrorLogger('no first_name available in individual contact [' . ($objectId ?? '') . ']');
    return;
  }

  $firstNameClean = get_last_string_part(" ", $firstName);
  if ( $firstNameClean === false ) {
    $callableErrorLogger('could not retrieve last part of first_name [' . $firstName . ']');
    return;
  }

  $numFirstNames = $callableQuery($firstNameClean);
  if ( $numFirstNames === false ) {
    $callableErrorLogger('sql query of occurence of first_name in contacts failed');
    return;
  }

  $prefix = generate_one_of_prefix($numFirstNames);
  if ( $prefix === false ) {
    $callableErrorLogger('could not generate prefix');
    return;
  }

  $newFirstName = prepend_to_string($prefix, ' ', $firstNameClean);
  if ( $newFirstName === false ) {
    $callableErrorLogger('could not prepend prefix to first_name');
    return;
  }
  // Civi::log()->debug("new first_name: " . $newFirstName);
  $objectRef['first_name'] = $newFirstName;
}

?>
